﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task7
{
    internal class MyClass<T>
    {
        public static T FactoryMethod()
        {
            try
            {
                return Activator.CreateInstance<T>(); // Створення нового екземпляру
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return default(T);
            }
        }
    }
}
