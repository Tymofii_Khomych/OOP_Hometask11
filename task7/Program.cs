﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int number = MyClass<int>.FactoryMethod();
            Console.WriteLine("Number: " + number);

            string text = MyClass<string>.FactoryMethod();
            Console.WriteLine("Text: " + text);

            // Тип, який підставляється під T, може бути будь-яким типом даних або класом.
            // Основний вимогою для типу T є те, що він повинен бути доступний для створення екземплярів, тобто повинен мати доступний конструктор без параметрів.
        }
    }
}
