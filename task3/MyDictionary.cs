﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class MyDictionary<TKey, TValue>
    {
        List<TKey> keys = new List<TKey>();
        List<TValue> values = new List<TValue>();

        public int size { get => keys.Count; }
        public void Add(TKey key, TValue value)
        {
            keys.Add(key);
            values.Add(value);
        }

        public string this[int  index]
        {
            get => index < size && index > 0 ? keys[index].ToString() + ": " + values[index].ToString() : "Not found";
        }
    }
}
