﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();

            dictionary.Add("apple", "яблоко");
            dictionary.Add("banana", "банан");
            dictionary.Add("blueberry", "черника");


            Console.WriteLine(dictionary[10]);
            Console.WriteLine(dictionary[-10]);
            Console.WriteLine(dictionary[2]);

            Console.WriteLine($"\nNumber of elements: {dictionary.size}");
        }
    }
}
