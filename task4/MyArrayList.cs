﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class MyArrayList
    {

        private List<object> list = new List<object>();
        public int size { get => list.Count; }

        public void Add(object item)
        {
            list.Add(item);
        }

        public object this[int index]
        {
            get { return list[index]; }
        }

        public void Clear()
        {
            list.Clear();
        }
    }
}
