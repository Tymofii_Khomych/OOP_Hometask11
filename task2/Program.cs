﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car("Toyota Camry", "2021-05-01");
            Car car2 = new Car("Honda Civic", "2020-10-15");
            Car car3 = new Car("Ford Mustang", "2019-07-20");

            CarCollection cars = new CarCollection();

            cars.Add(car1);
            cars.Add(car2);
            cars.Add(car3);

            foreach (Car car in cars.car_collection)
            {
                car.Show();
                Console.WriteLine();
            }

            cars[2].Show();
            Console.WriteLine("\n" + cars.size);

            cars.Delete();
            Console.WriteLine("\nCleared list: " + cars.size);
        }
    }
}
