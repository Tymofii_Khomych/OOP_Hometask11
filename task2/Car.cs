﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Car
    {
        public string name { get; }
        public string manufacture_date { get; }

        public Car(string name, string manufacture_date)
        {
            this.name = name;
            this.manufacture_date = manufacture_date;
        }

        public void Show()
        {
            Console.WriteLine($"Car model: {name}");
            Console.WriteLine($"Manufacture date: {manufacture_date}");
        }
    }
}
