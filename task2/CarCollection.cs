﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class CarCollection
    {
        public List<Car> car_collection = new List<Car>();
        public int size { get => car_collection.Count; }

        public void Add(Car car)
        {
            car_collection.Add(car);
        }

        public void Add(string name, string date)
        {
            Car new_car = new Car(name, date);
            car_collection.Add(new_car);    
        }

        public Car this[int index]
        {
            get
            {
                return car_collection[index];
            }
        }

        public void Delete()
        {
            car_collection.Clear(); 
        }
    }
}
